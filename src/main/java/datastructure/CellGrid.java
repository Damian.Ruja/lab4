package datastructure;

import java.util.ArrayList;

import cellular.CellState;

import java.lang.IndexOutOfBoundsException;

public class CellGrid implements IGrid {

        int rows;
        int cols;
        CellState[][] isDeadorAlive;

    public CellGrid(int rows, int columns, CellState initialState) {

        this.rows=rows;
        this.cols = columns;
        this.isDeadorAlive = new CellState[rows][cols];
        for(int r = 0; r<rows ; r++){
            for(int c = 0; c<cols; c++){

                initialState=CellState.DEAD ;

            }

        }
	}
    

    @Override
    public int numRows() {

        return this.rows;
    }

    @Override
    public int numColumns() {
   
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {

        this.isDeadorAlive[row][column] = element;

        
    }

    @Override
    public CellState get(int row, int column) {

        return isDeadorAlive[row][column];

    }

    @Override
    public IGrid copy() {

        IGrid gridCopy = new CellGrid(rows, cols, CellState.DEAD);
        
        for(int r = 0; r<numRows(); r++){
            for(int c = 0; c<numColumns();c++){
            gridCopy.set(r, c, isDeadorAlive[r][c]);
                           
            }
        }


                
       
        
        return gridCopy;
    }
    
}
